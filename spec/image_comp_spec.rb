require "securerandom"
require "spec_helper"
require "timeout"
require "tmpdir"
require_relative "../image_comp.rb"

RSpec.describe "image composition" do
  context "with no files" do
    it "creates a new image file with descriptive name" do
      Dir.mktmpdir do |temp_dir|
        image_spec = { bg: "Classroom", beholder: "beth", beholderExp: "angry" }
        beholder_compose_image(image_spec, filename = nil, dir = temp_dir)
        expect(File).to exist("#{temp_dir}/Classroom_beth_angry.png")
      end
    end
    it "creates a new image file with specified name" do
      Dir.mktmpdir do |temp_dir|
        image_spec = { bg: "Classroom", beholder: "beth", beholderExp: "angry" }
        filename = SecureRandom.urlsafe_base64 + ".png"
        beholder_compose_image(image_spec, filename, dir = temp_dir)
        expect(File).to exist("#{temp_dir}/" + filename)
      end
    end
  end
end

require 'spec_helper'
require 'rack/test'
require 'timeout'
require_relative '../sms_beholder.rb'

describe SMSBeholder do
  include Rack::Test::Methods

  def app
    SMSBeholder.new
  end

  before(:example) do | example |
    puts "\n*** #{example.location} ***\n"
    @dude = "Dude" + example.id.hash.to_s
    @dude2 = "Dude" + (example.id.hash + 1).to_s
  end

  it 'responds with initial state' do
    get '/', From: @dude

    expect(last_response.body).to include('Please tell me your name.')
  end

  it 'responds with initial state for each player' do
    get '/', From: @dude
    get '/', From: @dude2

    expect(last_response.body).to include('Please tell me your name.')
  end

  it 'moves to second state' do
    get '/', From: @dude
    get '/', From: @dude

    expect(last_response.body).to include('Day One.')
  end

  it 'can handle states with no story' do
    get '/', From: @dude, SkipToState: 'DAY2'
    get '/', From: @dude

    expect(last_response.body).to include("You arrive at school early")
  end

  it 'accepts a player name' do
    get '/', From: @dude
    get '/', From: @dude, Body: 'NameOfDude35'
    get '/', From: @dude
    get '/', From: @dude

    expect(last_response.body).to include("You're NameOfDude35, a student at Beholder High.")
  end

  it 'uses the right player name' do
    get '/', From: @dude
    get '/', From: @dude, Body: 'NumberFive'
    get '/', From: @dude2
    get '/', From: @dude2, Body: 'NumberOne'
    get '/', From: @dude
    get '/', From: @dude

    expect(last_response.body).to include("You're NumberFive, a student at Beholder High.")
  end

  it 'lets you skip to a different state for testing' do
    get '/', From: @dude, SkipToState: 'D1S107'

    expect(last_response.body).to include('Your brooding has distracted you')
  end

  it 'presents selections to player' do
    get '/', From: @dude, SkipToState: 'D1S108'

    expect(last_response.body).to include("what is the solution?\n1) Six\n2) Sixteen\n3) I wasn't paying attention")
  end

  it 'respects selections from player' do
    get '/', From: @dude, SkipToState: 'D1S108'
    get '/', From: @dude, Body: '1'

    expect(last_response.body).to include('Wrong. You haven\'t been paying attention, have you?')
  end

  it 'rejects non-numeric selections from player' do
    get '/', From: @dude, SkipToState: 'D1S108'
    get '/', From: @dude, Body: 'Dan'

    expect(last_response.body).to include("what is the solution?\n1) Six\n2) Sixteen\n3) I wasn't paying attention")
  end

  it 'rejects invalid selections from player' do
    get '/', From: @dude, SkipToState: 'D1S108'
    get '/', From: @dude, Body: '4'

    expect(last_response.body).to include("what is the solution?\n1) Six\n2) Sixteen\n3) I wasn't paying attention")
  end

  it 'shows constant selections from conditional selection set' do
    get '/', From: @dude, SkipToState: 'M00'

    expect(last_response.body).to include("1) How do you like soccer?\n2) Do you like dancing?")
  end

  it 'hides conditional selection without condition' do
    get '/', From: @dude, SkipToState: 'M00'

    expect(last_response.body).not_to include("that killer bicycle kick")
  end

  it 'hides conditional selection with failed condition' do
    get '/', From: @dude, SkipToState: 'M00', SetKey: 'mox-kick', SetKeyValue: 'unknown'

    expect(last_response.body).to include("1) How do you like soccer?\n2) Do you like dancing?")
    expect(last_response.body).not_to include("that killer bicycle kick")
  end

  it 'shows conditional selection with passed condition' do
    get '/', From: @dude, SkipToState: 'M00', SetKey: 'mox-kick', SetKeyValue: 'known'

    expect(last_response.body).to include("1) How do you like soccer?\n2) Do you like dancing?")
    expect(last_response.body).to include("3) So I was wondering if you could show me that killer bicycle kick")
  end

  it 'allows conditional selection with passed condition' do
    get '/', From: @dude, SkipToState: 'M00', SetKey: 'mox-kick', SetKeyValue: 'known'
    get '/', From: @dude, Body: '3'

    expect(last_response.body).to include("Do you think you can keep up with me?")
  end

  it 'disallows conditional selection without condition' do | example |
    get '/', From: @dude, SkipToState: 'M00'
    get '/', From: @dude, Body: '3'

    expect(last_response.body).to include("1) How do you like soccer?")
  end

  it 'disallows conditional selection with failed condition' do
    get '/', From: @dude, SkipToState: 'M00', SetKey: 'mox-kick', SetKeyValue: 'unknown'
    get '/', From: @dude, Body: '3'

    expect(last_response.body).to include("1) How do you like soccer?")
  end

  it 'shows ifConditionFalse story without condition' do
    get '/', From: @dude, SkipToState: 'S09'

    expect(last_response.body).to include("But it's a really cool car!")
    expect(last_response.body).not_to include("I'm always nervous when we hang out.")
  end

  it 'shows ifConditionFalse story with false condition' do
    get '/', From: @dude, SkipToState: 'S09', SetKey: 'siri-smoked', SetKeyValue: 'false'

    expect(last_response.body).to include("But it's a really cool car!")
    expect(last_response.body).not_to include("I'm always nervous when we hang out.")
  end

  it 'shows ifConditionTrue story with true condition' do
    get '/', From: @dude, SkipToState: 'S09', SetKey: 'siri-smoked', SetKeyValue: 'true'

    expect(last_response.body).to include("I'm always nervous when we hang out.")
    expect(last_response.body).not_to include("But it's a really cool car!")
  end

  it 'proceeds to ifConditionFalse state without condition' do
    get '/', From: @dude, SkipToState: 'S09'
    get '/', From: @dude

    expect(last_response.body).to include("Can you like, buzz off?")
  end

  it 'proceeds to ifConditionFalse state with false condition' do
    get '/', From: @dude, SkipToState: 'S09', SetKey: 'siri-smoked', SetKeyValue: 'false'
    get '/', From: @dude

    expect(last_response.body).to include("Can you like, buzz off?")
  end

  it 'proceeds to ifConditionTrue state with true condition' do
    get '/', From: @dude, SkipToState: 'S09', SetKey: 'siri-smoked', SetKeyValue: 'true'
    get '/', From: @dude

    expect(last_response.body).to include("Hehe. You're such a nerd")
  end

  it 'sets conditions at plot points' do
    get '/', From: @dude, SkipToState: 'D1S307'
    get '/', From: @dude, SkipToState: 'S02'

    expect(last_response.body).to include("hang out by the dumpster.")    
  end

  it 'can be played all the way through' do
    get '/', From: @dude
    Timeout::timeout(10) do
      until last_response.body.include?('And you won\'t be alone...') ||
        last_response.body.include?('Royalty does not concern itself with common dances.') ||
        last_response.body.include?('True power is slow dancing with someone who could beat you senseless.') ||
        last_response.body.include?('Thirst for knowledge. Hunger for power. No feast is fine enough.') do
        get '/', From: @dude, Body: ['1','2','3'].sample
        puts last_response.body
      end
    end
  end

  it 'can be played to a good ending' do
    get '/', From: @dude
    Timeout::timeout(10) do
      until last_response.body.include?('Royalty does not concern itself with common dances.') ||
            last_response.body.include?('True power is slow dancing with someone who could beat you senseless.') ||
            last_response.body.include?('Thirst for knowledge. Hunger for power. No feast is fine enough.') do
        @dude += '0' if last_response.body.include?('And you won\'t be alone...')
        get '/', From: @dude, Body: ['1','2','3'].sample
        puts last_response.body
      end
    end
  end

  it 'can be played to a bad ending' do
    get '/', From: @dude
    Timeout::timeout(10) do
      until last_response.body.include?('And you won\'t be alone...') do
        if last_response.body.include?('Royalty does not concern itself with common dances.') ||
           last_response.body.include?('True power is slow dancing with someone who could beat you senseless.') ||
           last_response.body.include?('Thirst for knowledge. Hunger for power. No feast is fine enough.')
          @dude += '0'
        end
        get '/', From: @dude, Body: %w[1 2 3].sample
        puts last_response.body
      end
    end
  end
end

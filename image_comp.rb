require 'json'
require 'rmagick'
include Magick

def beholder_compose_image(image_spec, filename=nil, dir="composite")
  if image_spec[:bg]
    bg_image_name = "images/bg/#{image_spec[:bg]}.jpg"
    return unless File.file?(bg_image_name)
    bg_image = ImageList.new(bg_image_name).resize(600, 400)
    new_image = bg_image
    if image_spec[:beholder]
      beholder_image_name = "images/char/#{image_spec[:beholder]}/#{image_spec[:beholder]}-" \
        "#{image_spec[:beholderExp]}.png"
      return unless File.file?(beholder_image_name)
      beholder_image = ImageList.new(beholder_image_name)
      new_image = bg_image.composite(beholder_image, CenterGravity, SrcOverCompositeOp)
      debug_text(image_spec, new_image) if image_spec[:debug]
    end
    if filename
      new_image.write("#{dir}/#{filename}")
    else
      new_image.write("#{dir}/#{image_spec[:bg]}_#{image_spec[:beholder]}_" \
                      "#{image_spec[:beholderExp]}#{image_spec[:debug] ? '_debug' : ''}.png")
    end
  else
    print("Bad image_spec: #{image_spec}")
  end
end

def debug_text(image_spec, image)
  text = Magick::Draw.new
  text.font = "Helvetica"
  text.pointsize = 30
  text.gravity = Magick::SouthGravity
  new_text = "#{image_spec[:beholder]} is #{image_spec[:beholderExp]}" \
    " at #{image_spec[:bg]}"
  text.fill = 'gray83'
  text.annotate(image, 0, 0, 2, 2, new_text)
  text.fill = 'gray40'
  text.annotate(image, 0, 0, -1.5, -1.5, new_text)
  text.fill = 'darkred'
  text.annotate(image, 0, 0, 0, 0, new_text)
end

@states = {}
@states = JSON.parse(File.open('script.json', 'r').read)
@states.each do |state|
  statename = state[0]
  puts("Statename: #{statename}")
  puts("State: #{state}")
  puts("Background: #{state[1]['bg']}")
  image_spec = state[1].transform_keys(&:to_sym)
  puts("Image spec: #{image_spec}")
  beholder_compose_image(image_spec, "#{statename}.png")
  image_spec[:debug] = true
  beholder_compose_image(image_spec, "#{statename}_debug.png")
end

# image_spec = {}
# %w[Classroom Dumpster GasStation Gym Library Lunchroom SoccerField StoreInterior].each do |bg|
#   image_spec[:bg] = bg
#   %w[beth mox siri].each do |beholder|
#     image_spec[:beholder] = beholder
#     %w[angry angry1 angry2 happy neutral shy].each do |exp|
#       image_spec[:beholderExp] = exp
#       # uncomment for debug images:
#       # image_spec[:debug] = true
#       beholder_compose_image(image_spec)
#     end
#   end
# end

# rubocop:disable Metrics/ClassLength, Metrics/BlockLength
require 'json'
require 'sinatra/base'

# Sinatra-derived class for easy testing
class SMSBeholder < Sinatra::Base
  configure do
    set :bind, '0.0.0.0'
    set :port, '14567'

    # Don't log them. We'll do that ourself
    set :dump_errors, false

    # Don't capture any errors. Throw them up the stack
    set :raise_errors, true

    # Disable internal middleware for presenting errors
    # as useful HTML pages
    set :show_exceptions, false
  end

  # The set of game states. After being loaded from a JSON file, should never change.
  module GameStates
    @states = {}

    def self.load
      @states = JSON.parse(File.open('script.json', 'r').read)
    end

    def self.states
      @states
    end

    def self.story_conditions_met(state, player)
      ret = true
      @states[state]['conditionalStory']['condition'].each_pair do |key, value|
        puts "Checking player #{player} for key #{key} value #{value}"
        unless player[key] == value
          puts '  nope!'
          ret = false
        end
      end
      ret
    end

    def self.selection_conditions_met(state, player)
      ret = true
      @states[state]['conditionalSelection']['condition'].each_pair do |key, value|
        puts "Checking player #{player} for key #{key} value #{value}"
        unless player[key] == value
          puts '  nope!'
          ret = false
        end
      end
      ret
    end

    def self.conditional_story(state, player)
      if story_conditions_met(state, player)
        @states[state]['conditionalStory']['ifConditionTrue']['story']
      else
        @states[state]['conditionalStory']['ifConditionFalse']['story']
      end
    end

    def self.conditional_next(state, player)
      if story_conditions_met(state, player)
        @states[state]['conditionalStory']['ifConditionTrue']['next']
      else
        @states[state]['conditionalStory']['ifConditionFalse']['next']
      end
    end

    def self.story(state, player)
      story = '' + @states[state].fetch('story', '')
      story += conditional_story(state, player) if @states[state].key?('conditionalStory')
      story
    end

    def self.next(state, player)
      next_state = @states[state].fetch('next', nil)
      next_state = conditional_next(state, player) if @states[state].key?('conditionalStory')
      next_state
    end

    def self.selections(state, player)
      selections = {}
      @states[state].fetch('selection', []).each do |selection|
        selections[selection[0]] = selection[1]
      end
      selections.merge(conditional_selections(state, player))
    end

    def self.conditions_to_get(state, player)
      [
        'constants',
        (selection_conditions_met(state, player) ? 'ifConditionTrue' : 'ifConditionFalse')
      ]
    end

    def self.conditional_selections(state, player)
      return {} unless @states[state].key?('conditionalSelection')

      sel_hash = @states[state]['conditionalSelection'].select do |k, _v|
        conditions_to_get(state, player).include?(k)
      end.values

      sel_hash = Hash[*sel_hash.collect(&:to_a).flatten] # to get to one hash
      sel_hash # was: selections
    end

    def self.plot_points(state)
      if @states[state] && @states[state].key?('plotPoint')
        @states[state]['plotPoint']
      else
        {}
      end
    end

    def self.same_image?(state1, state2)
      puts "Comparing states #{state1} and #{state2}..."
      return false if @states[state1].nil? || @states[state2].nil?

      @states[state1].fetch('bg', nil) == @states[state2].fetch('bg', nil) &&
        @states[state1].fetch('beholder', nil) == @states[state2].fetch('beholder', nil) &&
        @states[state1].fetch('beholderPos', nil) == @states[state2].fetch('beholderPos', nil) &&
        @states[state1].fetch('beholderExp', nil) == @states[state2].fetch('beholderExp', nil)
    end

    def self.needs_input?(state)
      @states[state].key?('textEntry') || @states[state].key?('selection') ||
        @states[state].key?('conditionalSelection')
    end
  end

  player_data = {}
  GameStates.load

  def process_misc(params, player)
    if params.key?('SetKey')
      puts "setting key #{params['SetKey']} to #{params['SetKeyValue']}"
      player[params['SetKey']] = params['SetKeyValue']
    end
    player
  end

  def process_input(params, player)
    puts "Processing input for player #{player} params #{params}"

    body = params['Body'] || 'NoBody'

    ## State is the state that was last shown
    state = player['state']

    # Handle input
    if GameStates.states[state].key?('textEntry')
      text_entry_key = GameStates.states[state]['textEntry']['key']
      puts "Player #{player} State #{state} adding text Key #{text_entry_key} value #{body}"
      player[text_entry_key] = String.new(body)
    end

    ## State transition
    player['state'] = next_state(params, player, body)

    player
  end

  def next_state(params, player, body)
    state = player['state']
    if params.key?('SkipToState')
      state = params['SkipToState']
    elsif GameStates.states[state].key?('selection') ||
          GameStates.states[state].key?('conditionalSelection')
      # handle using the input to choose selection
      if (index = body.to_i) != 0
        selections = GameStates.selections(state, player).to_a
        if selections.length >= index
          state = selections[index - 1].last
        else
          puts "selections.length #{selections.length} index #{index}"
          puts "Player selected #{body} but selections are #{selections}"
        end
      else
        puts "Player gave non-numeric selection: #{body}"
      end
    elsif GameStates.next(state, player)
      state = GameStates.next(state, player)
    else
      puts "Not sure where to go from state: #{GameStates.states[state]}"
    end
    state
  end

  def process_output(player)
    state = player['state']

    response = GameStates.story(state, player)
    GameStates.selections(state, player).each_with_index do |selection, index|
      response += "\n#{index + 1}) #{selection.first}"
    end

    GameStates.plot_points(state).each_pair do |key, value|
      player[key] = value
      puts "Plot point set player key #{key} to #{value}, and now player is #{player}"
    end

    # Fill in values in response
    player.keys.each do |key|
      puts "Filling in key #{key} with value #{player[key]}"
      response.gsub!(":#{key}:", player[key])
    end

    puts "Response is #{response}"
    response
  end

  get '/' do
    source = params['From']

    # New player?
    if player_data.key?(source)
      puts "Player #{source} has data #{player_data[source]}"
      player_data[source] = process_misc(params, player_data[source])
      player_data[source] = process_input(params, player_data[source])
    else
      puts "Player #{source} is a new player!"
      player_data[source] = {}
      player_data[source]['state'] = params['SkipToState'] || 'SPLASH'
      player_data[source] = process_misc(params, player_data[source])
    end

    response = process_output(player_data[source])

    response
  end

  post '/sms' do
    source = params['From']

    if player_data.key?(source)
      puts "Player #{source} has data #{player_data[source]}"
      player_data[source]['prev_state'] = player_data[source]['state']
      player_data[source] = process_misc(params, player_data[source])
      player_data[source] = process_input(params, player_data[source])
    else
      puts "Player #{source} is a new player!"
      player_data[source] = {}
      player_data[source]['prev_state'] = 'new'
      player_data[source]['state'] = params['SkipToState'] || 'SPLASH'
      player_data[source] = process_misc(params, player_data[source])
    end

    response = '<?xml version="1.0" encoding="UTF-8"?>' \
    '<Response>'

    response_body = process_output(player_data[source])
    image_state = player_data[source]['state']
    while response_body.length < 300 &&
          !GameStates.needs_input?(player_data[source]['state']) &&
          GameStates.same_image?(image_state,
                                 GameStates.next(player_data[source]['state'], player_data[source]))
      player_data[source]['prev_state'] = player_data[source]['state']
      player_data[source] = process_misc(params, player_data[source])
      player_data[source] = process_input(params, player_data[source])
      response_body += "\n" + process_output(player_data[source])
    end
    if player_data[source]['state'].include?('WIN') ||
       player_data[source]['state'].include?('FAILURE')
      response_body += "\nTHE END"
      player_data.delete(source)
    end
    response += '<Message>' \
               "<Body>#{response_body}</Body>"
    puts "Using image #{image_state}"
    response += '<Media>https://301days.com/assets/beholder_high/' \
                "#{image_state}.png</Media>"
    response += '</Message>'

    response += '</Response>'

    puts response
    response
  end

  run! if app_file == $PROGRAM_NAME
end

# rubocop:enable Metrics/ClassLength, Metrics/BlockLength
